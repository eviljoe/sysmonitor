/*
 * sysmonitor
 * Copyright (C) 2019  Joe Falascino
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use std::fs;
use std::fs::File;
use std::io::BufReader;
use std::io::prelude::*;
use std::path::PathBuf;

extern crate dirs;

/* ********* */
/* Constants */
/* ********* */

const PROC_MEM_FILE: &str = "/proc/meminfo";
const PROC_CPU_FILE: &str = "/proc/stat";
const OLD_CPU_STAT_FILE: &str = "old-cpu-stat.txt";
const OLD_CPU_USAGE_FILE: &str = "old-cpu-usage.txt";

const CPU_STAT_MIN_TIME: u64 = 1000;
const CPU_STAT_MAX_TIME: u64 = 8000;

/* ******* */
/* Structs */
/* ******* */

#[derive(Clone, Copy)]
struct MemInfo {
    total: Option<u64>,
    available: Option<u64>,
}

#[derive(Clone, Copy)]
struct CPUInfo {
    idle: Option<u64>,
    total: u64,
}

impl CPUInfo {
    pub fn new(
        user: Option<u64>,
        system: Option<u64>,
        nice: Option<u64>,
        idle: Option<u64>,
        wait: Option<u64>,
        irq: Option<u64>,
        srq: Option<u64>,
        zero: Option<u64>,
    ) -> CPUInfo {
        let total = user.unwrap_or(0) +
            system.unwrap_or(0) +
            nice.unwrap_or(0) +
            idle.unwrap_or(0) +
            wait.unwrap_or(0) +
            irq.unwrap_or(0) +
            srq.unwrap_or(0) +
            zero.unwrap_or(0);

        CPUInfo { idle, total }
    }

    pub fn has_usage_fields(&self) -> bool {
        return self.idle.is_some() && self.total > 0;
    }
}

/* *********** */
/* Main Method */
/* *********** */

fn main() {
    println!(
        "Mem: {}, CPU: {}",
        match get_memory_usage() {
            Some(mem) => mem.to_string(),
            None => "--".to_string()
        },
        match get_cpu_usage() {
            Some(cpu) => cpu.to_string(),
            None => "--".to_string()
        }
    );
}

/* ********************** */
/* Memory Usage Functions */
/* ********************** */

fn get_memory_usage() -> Option<u8> {
    let mut usage: Option<u8> = None;
    let mem = read_meminfo();

    if mem.total.is_some() && mem.available.is_some() {
        usage = Some(calculate_usage(mem.total.unwrap(), mem.available.unwrap()));
    }

    return usage;
}

fn read_meminfo() -> MemInfo {
    let mut mem_total: Option<u64> = None;
    let mut mem_available: Option<u64> = None;
    let mut mem_free: Option<u64> = None;

    let file = File::open(PROC_MEM_FILE).expect(&format!("Could not read memory file: {}", PROC_MEM_FILE));
    let br = BufReader::new(file);

    for line in br.lines().map(|l| l.expect("Could not read meminfo line")) {
        if line.starts_with("MemTotal: ") {
            mem_total = Some(get_memory_line_value(&line))
        } else if line.starts_with("MemAvailable: ") {
            mem_available = Some(get_memory_line_value(&line))
        } else if line.starts_with("MemFree: ") {
            mem_free = Some(get_memory_line_value(&line))
        }
    }

    // MemAvailable is not in Cygwin, so we have to use MemFree instead
    if mem_available.is_none() {
        mem_available = mem_free
    }

    return MemInfo { total: mem_total, available: mem_available };
}

fn get_memory_line_value(line: &String) -> u64 {
    return line.split_whitespace()
        .collect::<Vec<&str>>()
        .get(1)
        .unwrap()
        .parse::<u64>()
        .unwrap();
}

/* ******************* */
/* CPU Usage Functions */
/* ******************* */

fn get_cpu_usage() -> Option<u8> {
    let raw_stat_old = read_cpu_stat(&get_old_cpu_stat_file_path());
    let raw_stat_new = read_cpu_stat(&PROC_CPU_FILE.to_string());
    let cpu_info_old = get_cpu_info(&raw_stat_old);
    let cpu_info_new = get_cpu_info(&raw_stat_new);
    let mut usage: Option<u8> = None;

    if raw_stat_old.is_none() { // If there is no old CPU stat reading, create one
        write_last_cpu_stat(&raw_stat_new);
    } else if !can_calculate_cpu_usage(&cpu_info_old, &cpu_info_new) {
        // If there is not enough information to calculate the CPU usage.  This can happen if the old CPU info file is
        // empty.  The old file can end up empty sometimes if the terminal is closed while this program is running.
        write_last_cpu_stat(&raw_stat_new);
    } else { // If we have all the information needed to calculate the CPU usage
        usage = calculate_cpu_usage(&cpu_info_old, &cpu_info_new, &raw_stat_new)
    }

    return usage;
}

fn calculate_cpu_usage(cpu_info_old: &Option<CPUInfo>, cpu_info_new: &Option<CPUInfo>, raw_stat_new: &Option<String>) -> Option<u8> {
    let u_cpu_info_new = cpu_info_new.unwrap();
    let u_cpu_info_old = cpu_info_old.unwrap();
    let delta_total = (u_cpu_info_new.total as i64 - u_cpu_info_old.total as i64).abs() as u64;
    let mut usage: Option<u8> = None;

    if delta_total < CPU_STAT_MIN_TIME { // Too little time has passed between readings
        usage = read_last_cpu_usage();
    } else if delta_total > CPU_STAT_MAX_TIME { // Too much time has passed between readings
        write_last_cpu_stat(&raw_stat_new);
    } else { // An acceptable amount of time has passed between readings
        let delta_idle = (u_cpu_info_new.idle.unwrap() as i64 - u_cpu_info_old.idle.unwrap() as i64).abs() as u64;
        usage = Some(calculate_usage(delta_total, delta_idle));

        write_last_cpu_usage(&usage);
        write_last_cpu_stat(&raw_stat_new);
    }

    return usage;
}

fn can_calculate_cpu_usage(cpu_info_old: &Option<CPUInfo>, cpu_info_new: &Option<CPUInfo>) -> bool {
    return cpu_info_old.is_some() &&
        cpu_info_old.as_ref().unwrap().has_usage_fields() &&
        cpu_info_new.is_some() &&
        cpu_info_new.as_ref().unwrap().has_usage_fields();
}

fn read_cpu_stat(stat_file_path: &String) -> Option<String> {
    let mut cpu_stat: Option<String> = None;

    if fs::metadata(stat_file_path).is_ok() {
        cpu_stat = Some(match fs::read_to_string(stat_file_path) {
            Ok(cs) => cs,
            Err(error) => panic!("Could not read CPU stat file: file={}, error={}", stat_file_path, error)
        });
    }

    return cpu_stat;
}

fn write_last_cpu_stat(cpu_stat: &Option<String>) -> () {
    if let Some(u_cpu_stat) = cpu_stat {
        let mut path: PathBuf;

        make_config_dir();

        path = get_sysmonitor_config_dir();
        path.push(OLD_CPU_STAT_FILE);

        fs::write(&path, u_cpu_stat)
            .expect(&format!("Could not write old CPU stat file: {}", path.to_str().unwrap()));
    }
}

fn get_cpu_info(cpu_stat: &Option<String>) -> Option<CPUInfo> {
    let mut cpu_info: Option<CPUInfo> = None;

    if let Some(u_cpu_stat) = cpu_stat {
        for line in u_cpu_stat.lines() {
            if line.starts_with("cpu ") {
                cpu_info = Some(read_cpu_stat_line(&line.to_string()))
            }
        }
    }

    return cpu_info;
}

fn read_cpu_stat_line(line: &String) -> CPUInfo {
    let parts = line.split_whitespace().collect::<Vec<&str>>();
    let user = get_cpu_line_value(parts.get(1).map(|s| s.to_string()));
    let system = get_cpu_line_value(parts.get(2).map(|s| s.to_string()));
    let nice = get_cpu_line_value(parts.get(3).map(|s| s.to_string()));
    let idle = get_cpu_line_value(parts.get(4).map(|s| s.to_string()));
    let wait = get_cpu_line_value(parts.get(5).map(|s| s.to_string()));
    let irq = get_cpu_line_value(parts.get(6).map(|s| s.to_string()));
    let srq = get_cpu_line_value(parts.get(7).map(|s| s.to_string()));
    let zero = get_cpu_line_value(parts.get(8).map(|s| s.to_string()));

    return CPUInfo::new(user, system, nice, idle, wait, irq, srq, zero);
}

fn get_cpu_line_value(value: Option<String>) -> Option<u64> {
    return match value {
        Some(s) => s.parse::<u64>().ok(),
        None => None
    };
}

fn read_last_cpu_usage() -> Option<u8> {
    let mut usage: Option<u8> = None;
    let mut path = get_sysmonitor_config_dir();

    path.push(OLD_CPU_USAGE_FILE);

    if fs::metadata(&path).is_ok() {
        usage = fs::read_to_string(&path)
            .expect(&format!("Could not read old CPU usage file: {}", path.to_str().unwrap()))
            .parse::<u8>()
            .ok();
    }

    return usage;
}

fn write_last_cpu_usage(usage: &Option<u8>) -> () {
    if let Some(u_usage) = usage {
        let mut path: PathBuf;

        make_config_dir();

        path = get_sysmonitor_config_dir();
        path.push(OLD_CPU_USAGE_FILE);

        fs::write(&path, u_usage.to_string())
            .expect(&format!("Could not write old CPU usage file: {}", path.to_str().unwrap()));
    }
}

fn get_old_cpu_stat_file_path() -> String {
    let mut path = get_sysmonitor_config_dir();

    path.push(OLD_CPU_STAT_FILE);

    return path.to_str().unwrap().to_string();
}

/* ***************** */
/* Utility Functions */
/* ***************** */

fn calculate_usage(total: u64, unused: u64) -> u8 {
    return ((total - unused) as f64 / unused as f64 * 100.0).round() as u8;
}

fn get_sysmonitor_config_dir() -> PathBuf {
    let mut path = dirs::home_dir().unwrap();

    path.push(".jns");
    path.push("sysmonitor");

    return path;
}

fn make_config_dir() -> () {
    let dir_path = get_sysmonitor_config_dir();

    if fs::metadata(&dir_path).is_err() {
        fs::create_dir_all(&dir_path).expect(&format!("Could not create config directory: {}", dir_path.to_str().unwrap()));
    }
}
